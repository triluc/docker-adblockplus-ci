# Docker Containers for testing / building Adblock Plus

This is a collection of [Dockerfiles][1] capable of building and testing
Adblock Plus. Each subfolder represents a specific node version, running on a
specific Debian version.

These images are meant to be used in [Adblock Plus][2]'s CI environment.

## CI features

The CI will attempt to perform a full build of the images and test them against
a specific version of [Adblock Plus][2].

By default, the `master` of [Adblock Plus][2] Plus will be used. To test the
images against another revision:
 * Visit the repository's [pipeline creator][3]
 * In the section `Variables`, enter the desired revision with
`ADBLOCKPLUS_REVISION` as the key.
 * Run the pipeline

[1]: https://docs.docker.com/engine/reference/builder/
[2]: https://gitlab.com/eyeo/adblockplus/adblockpluschrome
[3]: https://gitlab.com/eyeo/adblockplus/docker-adblockplus-ci/pipelines/new
